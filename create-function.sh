#!/bin/bash
# ./create-function.sh <function_name>
cd services
function_name=$1
cargo new --lib ${function_name}

echo 'serde = { version = "1.0", features = ["derive"] }' >> ./${function_name}/Cargo.toml
echo 'serde_json = "1.0"' >> ./${function_name}/Cargo.toml
echo 'wasm-bindgen = { version = "0.2", features = ["serde-serialize"] }' >> ./${function_name}/Cargo.toml

echo '' >> ./${function_name}/Cargo.toml

echo '[lib]' >> ./${function_name}/Cargo.toml
echo "name = \"${function_name}\"" >> ./${function_name}/Cargo.toml
echo 'path = "src/lib.rs"' >> ./${function_name}/Cargo.toml
echo 'crate-type =["cdylib"]' >> ./${function_name}/Cargo.toml

cp ../templates/lib.rs ./${function_name}/src/lib.rs

