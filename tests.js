
const hey = require("./services/hey/pkg/hey")
const yo = require("./services/yo/pkg/yo")

console.log(
  yo.handle(
    {
      text:"coucou",
      author:"sam"
    },
    {
      host: '0.0.0.0:8080',
      'content-type': 'application/json'
    },
    "/"
  )
)

// wasm.getWasmFunction(`./services/hey/pkg/hey_bg.wasm`)
console.log(
  hey.handle(
    {
      text:"coucou",
      author:"sam"
    },
    {
      host: '0.0.0.0:8080',
      'content-type': 'application/json'
    },
    "/"
  )
)
// wasm.getWasmFunction(`./services/yo/pkg/yo_bg.wasm`)
