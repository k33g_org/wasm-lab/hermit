
use wasm_bindgen::prelude::*;
use serde::{Serialize, Deserialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct Question {
    pub text: String,
    pub author: String,
}

#[derive(Serialize, Deserialize)]
pub struct Answer {
    pub host: String,
    pub url: String,
    pub content_type: String,
    pub message: String,
}

#[wasm_bindgen]
pub fn handle(body: JsValue, headers: JsValue , url: String) -> Result<JsValue, JsValue>  {

    let headers: HashMap<String, String> = headers.into_serde().unwrap();

    let host: String = headers.get("host").unwrap().to_string();
    let content_type: String = headers.get("content-type").unwrap().to_string();

    let question: Question = body.into_serde().unwrap();

    // serialize answer to JsValue
    let answer = Answer {
        host: host,
        url: url,
        content_type: content_type,
        message: String::from(
            format!("👋 text: {}, author: {}", question.text, question.author)
        ),
    };

    return Ok(JsValue::from_serde(&answer).unwrap())
}