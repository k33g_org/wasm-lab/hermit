

let functions = {
  hey: require("./services/hey/pkg/hey"),
  yo: require("./services/yo/pkg/yo")
}

const fastify = require('fastify')({ logger: true })

// 🧰 Initialize settings
httpPort = process.env.HTTPS_PORT || 8080
maxListeners = process.env.MAX_LISTENERS || 1000
// Avoid: `MaxListenersExceededWarning: Possible EventEmitter memory leak detected`
require('events').setMaxListeners(maxListeners)


fastify.post(`/:function_name`, async (request, reply) => {
  //console.log("👋 headers", request.headers)
  let functionName = request.params.function_name
  // handle the errors
  // promises
  // add headers to the response
  //return wasm.handle(request.body, request.headers, request.url)
  return functions[functionName].handle(request.body, request.headers, request.url)

})

const start = async () => {
  try {
    await fastify.listen(httpPort, "0.0.0.0")
    fastify.log.info(`server listening on ${fastify.server.address().port}`)

  } catch (error) {
    fastify.log.error(error)
  }
}
start()
